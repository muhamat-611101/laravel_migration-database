<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul');
            $table->string('isi');
            $table->integer('jawaban_id')->nullable();
            $table->integer('profil_id')->nullable();

            $table->unsignedBigInteger('jawaban1_idx');
            $table->unsignedBigInteger('profil1_idx');

            $table->foreign('jawaban1_idx')->references('id')->on('pertanyaan')->nullable()->onDelete('cascade');
            $table->foreign('profil1_idx')->references('id')->on('profil')->nullable()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan');
    }
}
