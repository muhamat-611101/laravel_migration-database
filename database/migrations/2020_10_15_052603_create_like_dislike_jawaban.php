<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_jawaban', function (Blueprint $table) {
            $table->unsignedBigInteger('profil1_idx');
            $table->unsignedBigInteger('jawaban1_idx');

            $table->foreign('profil1_idx')->references('id')->on('profil')->onDelete('cascade');
            $table->foreign('jawaban1_idx')->references('profil1_idx')->on('table_jawaban')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_jawaban');
    }
}
