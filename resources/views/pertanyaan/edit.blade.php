@extends('adminlte.master')
@section('title','Edit Pertanyaan')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="container" style="width: 85%;">
        <div class="card card-primary mt-3">
            <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan</h3>
            </div>
            <!-- form start -->
            <form role="form" action="/update/{{$data->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body mt-5">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" value="{{$data->judul}}" name="judul" placeholder="Masukkan Judul">
                    </div>
                    <div class="form-group">
                        <label for="pertanyaan">Pertanyaan</label>
                        <input type="text" class="form-control" id="pertanyaan" value="{{$data->isi}}" name="isi" placeholder="Masukkan Pertanyaan">
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="ml-4">
                    <button type="submit" class="btn btn-primary">Update Data</button>
                    <a class="btn btn-warning text-white ml-1" href="/pertanyaan">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
@endsection