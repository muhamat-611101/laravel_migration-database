<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/store', 'PertanyaanController@store');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::post('/edit', 'PertanyaanController@edit');
Route::put('/update/{id}', 'PertanyaanController@update');
Route::delete('/destroy/{id}', 'PertanyaanController@destroy');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
